package com.VirtualDiary.model.DataBaseManagment.modelFromDB;

//Обертка класса LessonDistribution. Связывает преподавателя и список предметов, которые он ведет в разных классах

public class LessonDistribution {

    private int ID;
    private Teacher teacher;
    private Lesson lesson;
    private Classes classes;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public Classes getClasses() {
        return classes;
    }

    public void setClasses(Classes classes) {
        this.classes = classes;
    }
}

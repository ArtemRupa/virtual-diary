package com.VirtualDiary.model.DataBaseManagment.modelFromDB.TeacherExtension;

import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Classes;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Lesson;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.LessonDistribution;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Teacher;
import org.w3c.dom.stylesheets.MediaList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TeacherLessonDistribution {

    private Teacher teacher;
    private Map<Classes, List<Lesson>> TeacherLessonMap = new HashMap<>();
    private Map<String, Classes> ClassMap =new HashMap<>();
    private List<LessonDistribution> listLessonDistribution;
    private LessonDistribution lessonDistribution;

    public LessonDistribution getLessonDistribution(Classes classes, Lesson lesson) {

        for(int i=0; i<listLessonDistribution.size(); i++){
            if(listLessonDistribution.get(i).getClasses().getName().equals(classes.getName()) == true &&
                    listLessonDistribution.get(i).getLesson().getNameLesson().equals(lesson.getNameLesson())==true &&
                    listLessonDistribution.get(i).getTeacher().getName().equals(teacher.getName()) == true)  {
                lessonDistribution=listLessonDistribution.get(i);
                break;
            }
        }
        return lessonDistribution;
    }

    public Map<Classes, List<Lesson>> getTeacherLessonList() { return TeacherLessonMap; }

    public void setTeacher(Teacher teacher) { this.teacher = teacher; }

    public Map<String, Classes> getClassList() {
/*        List<Classes> list = new ArrayList<>();
        for (Classes classes: ClassMap.values()) {
            list.add(classes);
        }*/
        return ClassMap;
    }

    public Teacher getTeacher() { return teacher; }

    public void setTeacherLessonMap(List<LessonDistribution> listLessonD) {
        this.listLessonDistribution=listLessonD;
        int i=0;
        while(i<listLessonD.size()) {
            Classes classes = listLessonD.get(i).getClasses();
            Lesson lesson = listLessonD.get(i).getLesson();
                if (ClassMap.get(classes.getName()) != null) {
                    TeacherLessonMap.get(ClassMap.get(classes.getName())).add(lesson);
                } else {
                    ClassMap.put(classes.getName(), classes);
                    List<Lesson> lessonList = new ArrayList<>();
                    lessonList.add(lesson);
                    TeacherLessonMap.put(classes, lessonList);
                }
            i++;
        }

    }
}

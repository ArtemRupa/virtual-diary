package com.VirtualDiary.model.DataBaseManagment.modelFromDB.TeacherExtension;

import com.VirtualDiary.model.DataBaseManagment.modelFromDB.*;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Tools.Tools;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TeacherGradeMap {

    private Teacher teacher;
    private Lesson lesson;
    private Classes classes;
    private Map<Integer, Map<String, Grades>> AllGradeMap = new HashMap<>();

    private List<Student> studentInClass = new ArrayList<>();                   //Лист с учениками в классе
    private List<String> dataList = new ArrayList<>();                          //Лист с датами
    private Map<Integer, Double> avarageGradeMap = new HashMap<>();              //Мапа со средними оценками по предметам
    private Map<Integer, List<Grades>> GradeMap = new HashMap<>();               //Мапа с парой "Код предмета"->"список оценок по предмету"


    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public Classes getClasses() {
        return classes;
    }

    public void setClasses(Classes classes) {
        this.classes = classes;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public List<Student> getStudentInClass() {
        return studentInClass;
    }

    public List<String> getDataList() {
        return dataList;
    }

    public Map<Integer, Double> getAvarageGradeMap() {
        return avarageGradeMap;
    }

    public Map<Integer, List<Grades>> getGradeMap() {
        return GradeMap;
    }

    public void setAllGradeMap(List<Grades> GradeList, List<Student> Students) {
        int i = 0, k = 0;

        Tools.setDataList(dataList);

        while (i < Students.size()) {
            AllGradeMap.put(Students.get(i).getID(), Tools.getNewDateGradeMap());
            studentInClass.add(Students.get(i));
            GradeMap.put(Students.get(i).getID(), new ArrayList<>());
            i++;
        }

        i = 0;

        while (i < GradeList.size()) {
            Student student = GradeList.get(i).getStudent();
            Grades grade = GradeList.get(i);
            String date = GradeList.get(i).getData();

            AllGradeMap.get(student.getID()).put(date, grade);
            i++;
        }
        i = 0;

        while (i < studentInClass.size()) {
            while (k < dataList.size()) {
                Grades grade = AllGradeMap.get(studentInClass.get(i).getID()).get(dataList.get(k));
                GradeMap.get(studentInClass.get(i).getID()).add(grade);
                k++;
            }

            avarageGradeMap.put(studentInClass.get(i).getID(),
                    Tools.GetAvarageGrade(GradeMap.get(studentInClass.get(i).getID())));
            k = 0;
            i++;

        }

    }



}

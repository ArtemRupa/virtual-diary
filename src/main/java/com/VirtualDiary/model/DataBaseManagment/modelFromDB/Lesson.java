package com.VirtualDiary.model.DataBaseManagment.modelFromDB;

public class Lesson {

    private int ID;
    private String name;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNameLesson() {
        return name;
    }

    public void setNameLesson(String name) {
        this.name = name;
    }
}


package com.VirtualDiary.model.DataBaseManagment.modelFromDB;

public class Grades {

    private int ID;
    private int grade;
    private String Data;
    private Lesson lesson;
    private Student student;


    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) { this.grade = grade-1; } //Вычитаем 1 потому что в enum классе mysql только 4 значения, а grade,
                                                                // при вводе в PreparedStat - это порядковый номер enum-значения в бд mysql;

    public Lesson getLesson() {
        return lesson;
    }

    public void setLesson(Lesson lesson) {
        this.lesson = lesson;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}

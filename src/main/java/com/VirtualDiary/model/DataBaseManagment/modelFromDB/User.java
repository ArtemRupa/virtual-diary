package com.VirtualDiary.model.DataBaseManagment.modelFromDB;

import com.VirtualDiary.DAO.implementsDAO.EnumClass.RoleEnum;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Student;

public class User {
    String login;
    String password;
    RoleEnum role;
    int ID;

    public RoleEnum getRole() { return role; }

    public void setRole(RoleEnum role) { this.role = role; }

    public int getID() { return ID; }

    public void setID(int ID) { this.ID = ID; }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

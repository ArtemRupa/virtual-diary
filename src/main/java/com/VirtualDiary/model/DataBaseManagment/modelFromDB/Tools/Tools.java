package com.VirtualDiary.model.DataBaseManagment.modelFromDB.Tools;

//Служебный класс, сюда вынесена конфигурация даты и подсчет средних оценок. Далее сюда будут добавляться
//Другие инструменты для работы с данными

import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Grades;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Tools {

    public static void setDataList(List<String> dataList){
        dataList.add("2019-09-13");
        dataList.add("2019-09-14");
        dataList.add("2019-09-15");
        dataList.add("2019-09-16");
        dataList.add("2019-09-17");

    }

    public static Map<String, Grades> getNewDateGradeMap(){
        Map<String, Grades> dateGradeMap = new HashMap<>();
        dateGradeMap.put("2019-09-13", null);
        dateGradeMap.put("2019-09-14", null);
        dateGradeMap.put("2019-09-15", null);
        dateGradeMap.put("2019-09-16", null);
        dateGradeMap.put("2019-09-17", null);
        return dateGradeMap;
    }

    public static double GetAvarageGrade(List<Grades> gradeList){
        double avarage;
        int sum=0;
        double count=0;
        int i=0;
        DecimalFormat decimal = new DecimalFormat("#.00");

        while(i<gradeList.size()){
            if(gradeList.get(i)!=null && gradeList.get(i).getGrade()!=0) {
                sum = sum + gradeList.get(i).getGrade();
                count++;
            }
            i++;
        }

        avarage=sum/count;

        if(avarage>=2) {
            return Math.round(avarage);
        } else {
            return 0;
        }

    }
}

package com.VirtualDiary.model.DataBaseManagment.modelFromDB;

import com.VirtualDiary.DAO.implementsDAO.EnumClass.HasClassEnum;

import java.util.HashSet;
import java.util.Set;

public class Teacher {

    private int ID;
    private String Name;
    private int Category;
    private HasClassEnum HasClass;
    private int Telefon;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getCategory() {
        return Category;
    }

    public void setCategory(int category) {
        Category = category;
    }

    public HasClassEnum getHasClass() {
        return HasClass;
    }

    public void setHasClass(HasClassEnum hasClass) {
        HasClass = hasClass;
    }

    public int getTelefon() {
        return Telefon;
    }

    public void setTelefon(int telefon) {
        Telefon = telefon;
    }

}

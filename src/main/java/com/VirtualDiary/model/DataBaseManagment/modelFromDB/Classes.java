package com.VirtualDiary.model.DataBaseManagment.modelFromDB;

import com.VirtualDiary.DAO.implementsDAO.EnumClass.HasClassEnum;

import java.util.Objects;

public class Classes {

    private int ID;
    private String Name;
    private Teacher ClassroomTeacher;


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Teacher getClassroomTeacher() {
        return ClassroomTeacher;
    }

    public void setClassroomTeacher(Teacher classroomTeacher) {
        ClassroomTeacher = classroomTeacher;
        ID = classroomTeacher.getID();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Classes)) return false;
        Classes classes = (Classes) o;
        return ID == classes.ID &&
                Objects.equals(Name, classes.Name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, Name);
    }


}

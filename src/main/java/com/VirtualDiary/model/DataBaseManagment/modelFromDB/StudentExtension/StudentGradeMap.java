package com.VirtualDiary.model.DataBaseManagment.modelFromDB.StudentExtension;

import com.VirtualDiary.model.DataBaseManagment.modelFromDB.LessonDistribution;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Student;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Grades;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Lesson;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Tools.Tools;

import java.util.*;

public class StudentGradeMap {
    private Student student;
    private List<LessonDistribution> lessonInClass = new ArrayList<>();         //Лист с предметами в классе
    private List<String> dataList = new ArrayList<>();                          //Лист с датами
    private Map<Integer, Double> avarageGradeMap = new HashMap<>();              //Мапа со средними оценками по предметам
    private Map<Integer, List<Grades>> GradeMap = new HashMap<>();               //Мапа с парой предмет-список оценок по предмету
    private Map<Integer, Map<String, Grades>> AllGradeMap = new HashMap<>();     //Служебная мапа, которая позволяет доставать объект оценки, нужна, чтобы распределять объекты grade по листам

    public Map<Integer, List<Grades>> getGradeMap() { return GradeMap; }

    public List<LessonDistribution> getLessonInClass() { return lessonInClass; }

    public List<String> getDataList() { return dataList; }

    public Student getStudent() {
        return student;
    }

    public Map<Integer, Double> getAvarageGradeMap() { return avarageGradeMap; }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void setGradeMap(List<Grades> GradeList, List<LessonDistribution> LessonInClass) {
        int i=0, k=0;

        Tools.setDataList(dataList);

        while (i<LessonInClass.size()){
            AllGradeMap.put(LessonInClass.get(i).getLesson().getID(), Tools.getNewDateGradeMap());
            lessonInClass.add(LessonInClass.get(i));
            GradeMap.put(LessonInClass.get(i).getLesson().getID(), new ArrayList<>());
            i++;
        }
        i=0;


        while(i<GradeList.size()) {
            Lesson lesson = GradeList.get(i).getLesson();
            Grades grade = GradeList.get(i);
            String date = GradeList.get(i).getData();

            AllGradeMap.get(lesson.getID()).put(date, grade);

            i++;
        }
        i=0;


        while(i<lessonInClass.size()){
            while(k<dataList.size()){
                Grades grade = AllGradeMap.get(lessonInClass.get(i).getLesson().getID()).get(dataList.get(k));
                GradeMap.get(lessonInClass.get(i).getLesson().getID()).add(grade);
                k++;
            }

            avarageGradeMap.put(lessonInClass.get(i).getLesson().getID(),
                    Tools.GetAvarageGrade(GradeMap.get(lessonInClass.get(i).
                            getLesson().getID())));
            k=0;
            i++;
        }


    }
}

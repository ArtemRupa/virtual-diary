package com.VirtualDiary.model.DataBaseManagment;

import com.VirtualDiary.DAO.implementsDAO.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


//Основной класс для работы с БД. Содержит в себе все имплементации DAO interfaces, используется для взаимодействия с Бд.

@Component
@Scope("singleton")
public class TableManager {
    @Autowired
    private ClassesDaoImpl classesDao;
    @Autowired
    private TeacherDaoImpl teacherDao;
    @Autowired
    private LessonDaoImpl lessonDao;
    @Autowired
    private LessonDistributionDaoImpl lessonDistributionDao;
    @Autowired
    private StudentDaoImpl studentDao;
    @Autowired
    private GradesDaoImpl gradesDao;
    @Autowired
    private UsersDaoImpl usersDao;

    public StudentDaoImpl getStudentDao() { return studentDao; }
    public ClassesDaoImpl getClassesDao() {
        return classesDao;
    }
    public TeacherDaoImpl getTeacherDao() {
        return teacherDao;
    }
    public LessonDaoImpl getLessonDao() {
        return lessonDao;
    }
    public LessonDistributionDaoImpl getLessonDistributionDao() {
        return lessonDistributionDao;
    }
    public UsersDaoImpl getUsersDao() {
        return usersDao;
    }
    public GradesDaoImpl getGradesDao() { return gradesDao; }


    public TableManager(){}

}

package com.VirtualDiary.web;


//Кастомная структура, позволяющая более комфортно общаться через ajax.
//Передает обратно в ajax data -данные и status, анализируя который ajax выбирает дальнейшие действия

public class MessageResponse<T> {
        private String status;
        private T data;

    public String getStatus() { return status; }

    public T getData() { return data; }

    public MessageResponse(String status, T data) {
        this.status = status;
        this.data = data;
    }
}

package com.VirtualDiary.Application;


import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.*;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.TeacherExtension.TeacherGradeMap;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.TeacherExtension.TeacherLessonDistribution;
import com.VirtualDiary.web.MessageResponse;
import com.sun.org.apache.bcel.internal.generic.TargetLostException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.*;



@Controller("/teacher")
@PreAuthorize("hasAuthority('ROLE_TEACHER')")
public class TeacherController {

    @Autowired
    private TableManager tableManager;

    //В private переменные вынесены те переменные, с которыми постоянно работает контроллер и доступ к которым регулярно осуществялется
    //из маппингов.

    private Teacher teacher;        //Учитель, чья страница загружена
    private Lesson lesson;          //Предметы, которые он ведет
    private Classes classes;        //Классы, в которых он ведет эти предметы
    private List<Grades> listGradesForChange = new ArrayList<>();   //Лист оценок, которые он изменил


    @GetMapping("/teacher")
    public String teacher(Model model) {

        TeacherLessonDistribution teacherLessonDistribution =
                tableManager.getLessonDistributionDao().getTeacherLessonList(teacher);
        model.addAttribute("TeacherName", teacher.getName());
        model.addAttribute("Category", teacher.getCategory());

        model.addAttribute("Classes", new Classes());
        model.addAttribute("listOfClassesNames", teacherLessonDistribution.getClassList().keySet());

        return "teacher";
    }

    @PostMapping("/getClassesFromForm")
    public @ResponseBody ResponseEntity<Object> getClassesFromForm(@RequestParam String nameClasses){

        classes = tableManager.getClassesDao().findByName(nameClasses);

        TeacherLessonDistribution teacherLessonDistribution =
                tableManager.getLessonDistributionDao().getTeacherLessonList(teacher);

        List<Lesson> lessonList  =
                teacherLessonDistribution.getTeacherLessonList().get(classes);

        MessageResponse<List<Lesson>> response =
                new MessageResponse<>("success", lessonList);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }   //Получает имя класса из формы, преобразует его в объект и отправляет в ответ ajax'у список предметов, которые учитель ведет в данном классе

    @PostMapping("/getChangedGradeFromForm")
    public @ResponseBody HttpStatus addGradesInListForChange(@RequestParam int dataKey, @RequestParam int studentKey,
                                                             @RequestParam int grade){

        TeacherGradeMap teacherGradeMap =
                tableManager.getGradesDao().getGradeMapForTeacher(teacher, lesson, classes);
        String dataForGrade = teacherGradeMap.getDataList().get(dataKey);
        Student student = teacherGradeMap.getStudentInClass().get(studentKey);
        Grades newGrade = new Grades();

        newGrade.setGrade(grade);
        newGrade.setData(dataForGrade);
        newGrade.setLesson(lesson);
        newGrade.setStudent(student);

        listGradesForChange.add(newGrade);

        return HttpStatus.OK;
    }   //Ловит ajax запросы на изменение оценок и отправляет их в ListForChange

    @PostMapping("/getLessonFromForm")
    public @ResponseBody HttpStatus getLesson(@RequestParam String nameLesson){
        lesson = tableManager.getLessonDao().findByName(nameLesson);
        return HttpStatus.OK;
    }

    @PostMapping("/saveGrade")
    public String updateTable(Model model) {
        int count=0;
        while (count<listGradesForChange.size()) {
            Grades grade = listGradesForChange.get(count);
            Grades gradeFromDB = tableManager.getGradesDao().findByValue(grade); //Проверяем, существует ли оценка на эту дату в БД
            if (gradeFromDB != null) {                                             //Если существует, то удаляем старую оценку и вносим новую
                tableManager.getGradesDao().delete(gradeFromDB);
                if (grade.getGrade() != -1 && grade.getGrade()>1 && grade.getGrade()<=5) {  //чтобы избежать ошибки в бд мы перепроверяем значение из формы, чтобы оно вписывалось в область допустимых значений
                    tableManager.getGradesDao().insert(grade);
                }
            } else {
                if (grade.getGrade()>1 && grade.getGrade()<=5) {
                    tableManager.getGradesDao().insert(grade);
                }
            }
            count++;
        }

        writeGradeInTable(model);

        return "teacher";
    } //Обрабатывает все изменения оценок и передает их в БД


    @PostMapping("/getGrade")
    public String addTable(Model model){
        writeGradeInTable(model);
      return "teacher";
    }

    public void setTeacherByIdFromUser(int ID) {
        this.teacher = tableManager.getTeacherDao().findByID(ID);
    }

    private void writeGradeInTable(Model model){
        TeacherGradeMap teacherGradeMap =
                tableManager.getGradesDao().getGradeMapForTeacher(teacher, lesson, classes);

        model.addAttribute("DataList", teacherGradeMap.getDataList());
        model.addAttribute("Students", teacherGradeMap.getStudentInClass());
        model.addAttribute("GradeMap", teacherGradeMap.getGradeMap());
        model.addAttribute("AvarageGrade", teacherGradeMap.getAvarageGradeMap());

        model.addAttribute("readmeGrade", " Оценки учеников класса " + classes.getName() + " по предмету " + lesson.getNameLesson());

        model.addAttribute("TeacherName", teacher.getName());
        model.addAttribute("Category", teacher.getCategory());

        TeacherLessonDistribution teacherLessonDistribution =
                tableManager.getLessonDistributionDao().getTeacherLessonList(teacher);
        model.addAttribute("Classes", new Classes());
        model.addAttribute("listOfClassesNames", teacherLessonDistribution.getClassList().keySet());
    }       //Метод для заполнения таблицы. Вынесен отдельно, чтобы избежать повторений в коде.

}

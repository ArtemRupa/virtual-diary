package com.VirtualDiary.Application;

import com.VirtualDiary.model.DataBaseManagment.modelFromDB.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//Контроллер для переадресации на нужную страницу после успешной авторизации.
//С помощью set в контроллер передается user, затем ID user (код ученика или преподавателя) передается в контроллер teacher или student
//В зависимости от Role


@Controller
public class AuthController {
    @Autowired
    TeacherController teacherController;
    @Autowired
    StudentController studentController;

    private User user;

    @RequestMapping("/success")
        public void loginPageRedirect(HttpServletRequest request, HttpServletResponse response, Authentication authResult) throws IOException  {

            String role =  authResult.getAuthorities().toString();


            if(role.contains("ROLE_STUDENT")){
                response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/student"));
                studentController.setStudentByIdFromUser(user.getID());
            }
            else if(role.contains("ROLE_TEACHER")) {
                teacherController.setTeacherByIdFromUser(user.getID());
                response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/teacher"));
            }
        }

    public void setUser(User user) {
        this.user = user;
    }

}


package com.VirtualDiary.Application;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

//Контроллер главной страницы

@Controller("/home")
public class HomeController {

    @GetMapping("/home")
    public String getHome(Model model){
        return "home";
    }
}

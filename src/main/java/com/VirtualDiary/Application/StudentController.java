
package com.VirtualDiary.Application;

import com.VirtualDiary.model.DataBaseManagment.TableManager;

import com.VirtualDiary.model.DataBaseManagment.modelFromDB.*;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.StudentExtension.StudentGradeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Controller
@PreAuthorize("hasAuthority('ROLE_STUDENT')")
public class StudentController {

    @Autowired
    TableManager tableManager;

    private Student student;

    @GetMapping("/student")
    public String student(Model model) {

        StudentGradeMap studentGradeMap = tableManager.getGradesDao().getGradeMapForStudent(student);

        model.addAttribute("classes", student.getClasses().getName());
        model.addAttribute("boss", student.getClasses().getClassroomTeacher().getName());
        model.addAttribute("name", student.getName());

        model.addAttribute("DataList", studentGradeMap.getDataList());
        model.addAttribute("Lessons", studentGradeMap.getLessonInClass());
        model.addAttribute("GradeMap", studentGradeMap.getGradeMap());
        model.addAttribute("AvarageGrade", studentGradeMap.getAvarageGradeMap());

        return "student";
    }

    public void setStudentByIdFromUser(int ID) {
        this.student = tableManager.getStudentDao().findByID(ID);
    }

    public StudentController(){

    }

}

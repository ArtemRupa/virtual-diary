package com.VirtualDiary.DAO.interfacesDAO;

import com.VirtualDiary.model.DataBaseManagment.modelFromDB.User;

public interface UsersDAO {
    public User findUsers(String username);
}

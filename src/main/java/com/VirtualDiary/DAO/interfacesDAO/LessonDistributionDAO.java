package com.VirtualDiary.DAO.interfacesDAO;

import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Classes;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Lesson;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.LessonDistribution;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.TeacherExtension.TeacherLessonDistribution;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Teacher;

import java.util.List;

public interface LessonDistributionDAO {

    public TeacherLessonDistribution getTeacherLessonList(Teacher teacher);
    public List<LessonDistribution> findAll();
    public LessonDistribution findByID(int ID);
    public Teacher findByLessonInClass(Classes classes, Lesson lesson);
    public List<LessonDistribution> findByClass(Classes classes);
    public int insert(Classes cLasses, Teacher teacher, Lesson lesson);
    public void delete(LessonDistribution lessonDistribution);
}

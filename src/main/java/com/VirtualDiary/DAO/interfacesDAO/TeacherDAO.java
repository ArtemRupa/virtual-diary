package com.VirtualDiary.DAO.interfacesDAO;

import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Teacher;

import java.util.List;

public interface TeacherDAO {
    public List<Teacher> findAll();
    public List<Teacher> findByName(String Name);
    public Teacher findByID(int ID);
    public int insert(Teacher teacher);
    public void delete(Teacher teacher);
}

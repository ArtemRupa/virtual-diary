package com.VirtualDiary.DAO.interfacesDAO;

import com.VirtualDiary.model.DataBaseManagment.modelFromDB.*;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.StudentExtension.StudentGradeMap;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.TeacherExtension.TeacherGradeMap;

import java.util.List;

public interface GradesDAO {
    public List<Grades> findAll();
    public StudentGradeMap getGradeMapForStudent(Student student);
    public TeacherGradeMap getGradeMapForTeacher(Teacher teacher, Lesson lesson, Classes classes);
    public Grades findByID(int ID);
    public Grades findByValue(Grades grade);
    public int insert(Grades grades);
    public void delete(Grades grades);
}

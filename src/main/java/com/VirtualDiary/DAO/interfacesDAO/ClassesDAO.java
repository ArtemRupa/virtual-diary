package com.VirtualDiary.DAO.interfacesDAO;

import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Classes;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Teacher;

import java.util.List;

public interface ClassesDAO {
    public List<Classes> findAll();
    public Classes findByName(String Name);
    public Classes findByID(int id);
    public int insert(Teacher teacher, Classes classes);
    public void delete(Classes classes);
}

package com.VirtualDiary.DAO.interfacesDAO;

import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Lesson;

import java.util.List;

public interface LessonDAO {
    public List<Lesson> findAll();
    public Lesson findByName(String Name);
    public Lesson findByID(int ID);
    public int insert(Lesson lesson);
    public void delete(Lesson lesson);
}

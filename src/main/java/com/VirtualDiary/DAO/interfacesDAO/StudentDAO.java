package com.VirtualDiary.DAO.interfacesDAO;

import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Classes;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Student;


import java.util.List;

public interface StudentDAO {
    public List<Student> findAll();
    public List<Student> findByName(String Name);
    public Student findByID(int ID);
    public List<Student> findByClass(Classes classes);
    public int insert(Student student);
    public void delete(Student student);
}

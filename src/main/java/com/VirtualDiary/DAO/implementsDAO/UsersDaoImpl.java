package com.VirtualDiary.DAO.implementsDAO;

import com.VirtualDiary.DAO.implementsDAO.EnumClass.RoleEnum;
import com.VirtualDiary.DAO.interfacesDAO.UsersDAO;
import com.VirtualDiary.DAO.implementsDAO.RowMapperImpl.UsersRowMapper;
import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UsersDaoImpl implements UsersDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    private TableManager tableManager;

    @Override
    public User findUsers(String username) {
        User user;
        String SQL;
        SQL = "SELECT * FROM ученики_пользователи WHERE Логин=?";
        List<User> list_student = jdbcTemplate.query(SQL, new UsersRowMapper(tableManager),
                username);
        if(list_student.size()>0)
            {user = list_student.get(0);
            user.setRole(RoleEnum.STUDENT);
            return user;}
        else
            {
                SQL = "SELECT * FROM преподаватели_пользователи WHERE Логин=?";
                List<User> list_teacher = jdbcTemplate.query(SQL, new UsersRowMapper(tableManager),
                        username);
                if(list_teacher.size()>0)
                {user = list_teacher.get(0);
                user.setRole(RoleEnum.TEACHER);
                return user;}
                else
                {return null;}
            }
    }

/*    @Override
    public int insert(UsersStudent usersStudent) {
        String SQL = "INSERT INTO ученики_пользователи (Код_ученика, Логин, Пароль) VALUES (?,?,?)";
        jdbcTemplate.update(SQL, usersStudent.getStudent().getID(), usersStudent.getLogin(), usersStudent.getPassword());
        return usersStudent.getStudent().getID();
    }

    @Override
    public void delete(UsersStudent usersStudent) {
        String SQL = "DELETE FROM преподаватели_пользователи WHERE Код_ученика=?";

        jdbcTemplate.update(SQL, usersStudent.getStudent().getID());
    }*/
}

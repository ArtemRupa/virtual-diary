package com.VirtualDiary.DAO.implementsDAO;

import com.VirtualDiary.DAO.interfacesDAO.GradesDAO;
import com.VirtualDiary.DAO.implementsDAO.RowMapperImpl.GradesRowMapper;
import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.*;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.StudentExtension.StudentGradeMap;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.TeacherExtension.TeacherGradeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;


@Component
public class GradesDaoImpl implements GradesDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private TableManager tableManager;

    @Override
    public List<Grades> findAll() {
        String SQL = "SELECT * FROM оценки";

        List<Grades> list = jdbcTemplate.query(SQL, new GradesRowMapper(tableManager));

        return list;
    }

    @Override
    public Grades findByValue(Grades grade) {
        String SQL = "SELECT * FROM оценки WHERE Дата=? and Ученик=? and Предмет=?";

        List<Grades> list = jdbcTemplate.query(SQL, new GradesRowMapper(tableManager), grade.getData(),
                grade.getStudent().getID(), grade.getLesson().getID());
        if (list.size()>0) {
            return list.get(0);
        } else {return null;}
    }

    @Override
    public TeacherGradeMap getGradeMapForTeacher(Teacher teacher, Lesson lesson, Classes classes) {
        String SQL = "SELECT * FROM оценки RIGHT JOIN ученики ON оценки.Ученик = ученики.Код_ученика WHERE Предмет=? and Код_классного_руководителя=?";
        TeacherGradeMap teacherGradeMap = new TeacherGradeMap();
        teacherGradeMap.setTeacher(teacher);
        teacherGradeMap.setClasses(classes);
        teacherGradeMap.setLesson(lesson);
        List<Grades> gradesList = jdbcTemplate.query(SQL, new GradesRowMapper(tableManager), lesson.getID(), classes.getID());
        List<Student> studentList = tableManager.getStudentDao().findByClass(classes);
        teacherGradeMap.setAllGradeMap(gradesList, studentList);
        return teacherGradeMap;
    }

    @Override
    public StudentGradeMap getGradeMapForStudent(Student student) {
        String SQL = "SELECT * FROM оценки WHERE ученик=? ORDER BY Дата";
        StudentGradeMap studentGradeMap = new StudentGradeMap();
        studentGradeMap.setStudent(student);
        List<Grades> listGrades = jdbcTemplate.query(SQL, new GradesRowMapper(tableManager), student.getID());
        List<LessonDistribution> listLessonInClass = tableManager.getLessonDistributionDao().findByClass(student.getClasses());
        studentGradeMap.setGradeMap(listGrades, listLessonInClass);
        return studentGradeMap;
    }

    @Override
    public Grades findByID(int ID) {
        String SQL = "SELECT * FROM оценки WHERE Код=?";

        List<Grades> list = jdbcTemplate.query(SQL, new GradesRowMapper(tableManager), ID);
        Grades grade = list.get(0);

        return grade;
    }

    @Override
    public int insert(Grades grades) {

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement("INSERT INTO оценки (Оценка, Дата, Ученик, Предмет)" +
                        " VALUES (?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, grades.getGrade()+1);
                ps.setString(2, grades.getData());
                ps.setInt(3, grades.getStudent().getID());
                ps.setInt(4, grades.getLesson().getID());
                return ps;
            }
        }, keyHolder);

        grades.setID(keyHolder.getKey().intValue());

        return grades.getID();
    }

    @Override
    public void delete(Grades grade) {
        String SQL = "DELETE FROM оценки WHERE Код=?";

        jdbcTemplate.update(SQL, grade.getID());
    }
}

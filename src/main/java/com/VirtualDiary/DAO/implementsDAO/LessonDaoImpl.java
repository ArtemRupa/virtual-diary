package com.VirtualDiary.DAO.implementsDAO;

import com.VirtualDiary.DAO.interfacesDAO.LessonDAO;
import com.VirtualDiary.DAO.implementsDAO.RowMapperImpl.LessonRowMapper;
import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Lesson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;


@Component
public class LessonDaoImpl implements LessonDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private TableManager tableManager;

    @Override
    public List<Lesson> findAll() {
        String SQL = "SELECT * FROM предметы";

        List<Lesson> list = jdbcTemplate.query(SQL, new LessonRowMapper(tableManager));

        return list;
    }


    @Override
    public Lesson findByID(int ID) {
        String SQL = "SELECT * FROM предметы WHERE Код_предмета=?";

        List<Lesson> list = jdbcTemplate.query(SQL, new LessonRowMapper(tableManager), ID);
        Lesson lesson = list.get(0);

        return lesson;
    }


    @Override
    public Lesson findByName(String Name) {
        String SQL = "SELECT * FROM предметы WHERE Название=?";

        List<Lesson> listLesson = jdbcTemplate.query(SQL, new LessonRowMapper(tableManager), Name);

        Lesson lesson = listLesson.get(0);

        return lesson;
    }

    @Override
    public int insert(Lesson lesson) {
        String SQL = "INSERT INTO предметы (Название) VALUES (?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, lesson.getNameLesson());
                return ps;
            }
        }, keyHolder);

        lesson.setID(keyHolder.getKey().intValue());

        return lesson.getID();

    }

    @Override
    public void delete(Lesson lesson) {
        String SQL = "DELETE FROM предметы WHERE Код_предмета=?";

        jdbcTemplate.update(SQL, lesson.getID());
    }
}

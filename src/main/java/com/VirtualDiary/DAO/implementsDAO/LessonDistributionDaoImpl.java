package com.VirtualDiary.DAO.implementsDAO;

import com.VirtualDiary.DAO.interfacesDAO.LessonDistributionDAO;
import com.VirtualDiary.DAO.implementsDAO.RowMapperImpl.LessonDistributionRowMapper;
import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Classes;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Lesson;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Teacher;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.LessonDistribution;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.TeacherExtension.TeacherLessonDistribution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Component
public class LessonDistributionDaoImpl implements LessonDistributionDAO {
    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    private TableManager tableManager;

    @Override
    public TeacherLessonDistribution getTeacherLessonList(Teacher teacher) {

        String SQL = "SELECT * FROM распределение_предметов WHERE Код_преподавателя=?";
        TeacherLessonDistribution TLD =  new TeacherLessonDistribution();
        TLD.setTeacher(teacher);
        List<LessonDistribution> list = jdbcTemplate.query(SQL, new LessonDistributionRowMapper(tableManager), teacher.getID());
        TLD.setTeacherLessonMap(list);

        return TLD;
    }

    @Override
    public List<LessonDistribution> findAll() {
        String SQL = "SELECT * FROM распределение_предметов";
        List<LessonDistribution> list = jdbcTemplate.query(SQL, new LessonDistributionRowMapper(tableManager));
        return list;
    }

    @Override
    public Teacher findByLessonInClass(Classes classes, Lesson lesson) {
        String SQL = "SELECT * FROM Распределение_предметов WHERE Код_класса =? AND Код_предмета=?";
        List<LessonDistribution> list = jdbcTemplate.query(SQL, new LessonDistributionRowMapper(tableManager),
                classes.getID(), lesson.getID());
        LessonDistribution LD = list.get(0);

        return LD.getTeacher();
    }

    @Override
    public LessonDistribution findByID(int ID) {
        String SQL = "SELECT * FROM распределение_предметов WHERE Номер_связи=?";
        List<LessonDistribution> list = jdbcTemplate.query(SQL,
                new LessonDistributionRowMapper(tableManager), ID);
        return list.get(0);
    }

    @Override
    public List<LessonDistribution> findByClass(Classes classes) {
        String SQL = "SELECT * FROM распределение_предметов WHERE Код_класса=? ORDER BY Код_предмета";
        List<LessonDistribution> list = jdbcTemplate.query(SQL,
                new LessonDistributionRowMapper(tableManager), classes.getID());
        return list;
    }

    @Override
    public int insert(Classes classes, Teacher teacher, Lesson lesson) {
        String SQL = "INSERT INTO распределение_предметов (Код_класса, Код_преподавателя, Код_предмета) VALUES (?,?,?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, classes.getID());
                ps.setInt(2, teacher.getID());
                ps.setInt(3, lesson.getID());
                return ps;
            }
        }, keyHolder);

        return keyHolder.getKey().intValue();
    }

    @Override
    public void delete(LessonDistribution lessonDistribution) {
        String SQL = "DELETE FROM распределение_предметов WHERE Номер_связи=?";

        jdbcTemplate.update(SQL, lessonDistribution.getID());
    }
}

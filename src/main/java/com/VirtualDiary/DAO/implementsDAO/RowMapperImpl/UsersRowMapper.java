package com.VirtualDiary.DAO.implementsDAO.RowMapperImpl;

import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UsersRowMapper extends TableManager implements RowMapper<User> {

    private TableManager tableManager;

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        user.setLogin(resultSet.getString("Логин"));
        user.setPassword(resultSet.getString("Пароль"));
        user.setID(resultSet.getInt("Код"));
        return user;
    }

    public UsersRowMapper(TableManager tableManager) {
        this.tableManager = tableManager;
    }
}

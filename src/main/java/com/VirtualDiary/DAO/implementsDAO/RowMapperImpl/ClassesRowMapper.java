package com.VirtualDiary.DAO.implementsDAO.RowMapperImpl;

import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Classes;
import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Teacher;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class ClassesRowMapper implements RowMapper<Classes> {
    private TableManager tableManager;

    @Override
    public Classes mapRow(ResultSet resultSet, int i) throws SQLException {

        Classes classes = new Classes();
        classes.setName(resultSet.getString("Название"));
        int id = resultSet.getInt("Код_классного_руководителя");
        classes.setID(id);
        Teacher teacher = (tableManager.getTeacherDao().findByID(id));
        classes.setClassroomTeacher(teacher);
        return classes;
    }

    public ClassesRowMapper(TableManager tableManager){
        this.tableManager=tableManager;
    }
}

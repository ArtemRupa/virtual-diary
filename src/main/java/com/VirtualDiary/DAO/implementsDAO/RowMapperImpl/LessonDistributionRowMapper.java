package com.VirtualDiary.DAO.implementsDAO.RowMapperImpl;

import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.LessonDistribution;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LessonDistributionRowMapper implements RowMapper<LessonDistribution> {

    private TableManager tableManager;

    @Override
    public LessonDistribution mapRow(ResultSet resultSet, int i) throws SQLException {

        LessonDistribution LD = new LessonDistribution();

        LD.setID(resultSet.getInt("Номер_связи"));
        LD.setLesson(tableManager.getLessonDao().findByID(resultSet.getInt("Код_предмета")));
        LD.setTeacher(tableManager.getTeacherDao().findByID(resultSet.getInt("Код_преподавателя")));
        LD.setClasses(tableManager.getClassesDao().findByID(resultSet.getInt("Код_класса")));


        return LD;
    }

    public LessonDistributionRowMapper(TableManager tableManager) {
        this.tableManager = tableManager;
    }
}

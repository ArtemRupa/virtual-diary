package com.VirtualDiary.DAO.implementsDAO.RowMapperImpl;

import com.VirtualDiary.DAO.implementsDAO.EnumClass.HasClassEnum;
import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Teacher;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TeacherRowMapper implements RowMapper<Teacher> {

    private TableManager tableManager;

    @Override
    public Teacher mapRow(ResultSet resultSet, int i) throws SQLException {
        Teacher teach = new Teacher();
        teach.setID(resultSet.getInt("Код_преподавателя"));
        teach.setName(resultSet.getString("ФИО"));
        teach.setCategory(resultSet.getInt("Категория"));
        teach.setTelefon(resultSet.getInt("Телефон"));
        String HasClass =resultSet.getString("Классное_руководство");
        switch(HasClass){
            case "ДА": teach.setHasClass(HasClassEnum.ДА); break;
            case "НЕТ": teach.setHasClass(HasClassEnum.НЕТ); break;
            default: teach.setHasClass(HasClassEnum.НЕТ); break;
        }
        return teach;
    }

    public TeacherRowMapper(TableManager tableManager) {
        this.tableManager = tableManager;
    }
}

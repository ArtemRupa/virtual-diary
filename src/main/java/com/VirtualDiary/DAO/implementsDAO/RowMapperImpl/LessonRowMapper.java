package com.VirtualDiary.DAO.implementsDAO.RowMapperImpl;

import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Lesson;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LessonRowMapper implements RowMapper<Lesson> {

    private TableManager tableManager;

    @Override
    public Lesson mapRow(ResultSet resultSet, int i) throws SQLException {
        Lesson lesson = new Lesson();
        lesson.setID(resultSet.getInt("Код_предмета"));
        lesson.setNameLesson(resultSet.getString("Название"));
        return lesson;
    }

    public LessonRowMapper(TableManager tableManager) {
        this.tableManager = tableManager;
    }
}

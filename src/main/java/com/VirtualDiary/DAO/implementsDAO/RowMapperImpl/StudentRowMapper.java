package com.VirtualDiary.DAO.implementsDAO.RowMapperImpl;

import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Student;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentRowMapper implements RowMapper<Student> {

    private TableManager tableManager;

    @Override
    public Student mapRow(ResultSet resultSet, int i) throws SQLException {
        Student student = new Student();
        student.setID(resultSet.getInt("Код_ученика"));
        student.setName(resultSet.getString("ФИО"));
        student.setClasses(tableManager.getClassesDao().findByID(resultSet.getInt("Код_классного_руководителя")));
        return student;
    }

    public StudentRowMapper(TableManager tableManager) {
        this.tableManager = tableManager;
    }
}

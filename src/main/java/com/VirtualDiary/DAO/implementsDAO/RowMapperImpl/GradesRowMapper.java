package com.VirtualDiary.DAO.implementsDAO.RowMapperImpl;

import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Grades;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GradesRowMapper implements RowMapper<Grades> {

    private TableManager tableManager;

    @Override
    public Grades mapRow(ResultSet resultSet, int i) throws SQLException {
        Grades grade = new Grades();
        grade.setID(resultSet.getInt("Код"));
        grade.setStudent(tableManager.getStudentDao().findByID(resultSet.getInt("Ученик")));
        grade.setLesson(tableManager.getLessonDao().findByID(resultSet.getInt("Предмет")));
        grade.setData(resultSet.getString("Дата"));
        grade.setGrade(resultSet.getInt("Оценка")+1);
        return grade;
    }

    public GradesRowMapper(TableManager tableManager){
        this.tableManager=tableManager;
    }
}

package com.VirtualDiary.DAO.implementsDAO;

import com.VirtualDiary.DAO.interfacesDAO.StudentDAO;
import com.VirtualDiary.DAO.implementsDAO.RowMapperImpl.StudentRowMapper;
import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Classes;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
@Component
public class StudentDaoImpl implements StudentDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private TableManager tableManager;

    @Override
    public List<Student> findAll() {

        String SQL = "SELECT * FROM ученики";

        List<Student> list = jdbcTemplate.query(SQL, new StudentRowMapper(tableManager));

        return list;
    }

    @Override
    public List<Student> findByName(String Name) {
        String SQL = "SELECT * FROM ученики WHERE ФИО=?";

        List<Student> list = jdbcTemplate.query(SQL, new StudentRowMapper(tableManager), Name);

        return list;
    }

    @Override
    public Student findByID(int ID) {

        String SQL = "SELECT * FROM ученики WHERE Код_ученика=?";

        List<Student> list = jdbcTemplate.query(SQL, new StudentRowMapper(tableManager), ID);
        Student student = list.get(0);

        return student;
    }

    @Override
    public List<Student> findByClass(Classes classes) {
        String SQL = "SELECT * FROM ученики WHERE Код_классного_руководителя=?";
        List<Student> list = jdbcTemplate.query(SQL, new StudentRowMapper(tableManager), classes.getID());
        return list;
    }

    @Override
    public int insert(Student student) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement("INSERT INTO ученики (ФИО, Код_классного_руководителя)" +
                        " VALUES (?,?)", Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, student.getName());
                ps.setInt(2, student.getClasses().getID());

                return ps;
            }
        }, keyHolder);

        student.setID(keyHolder.getKey().intValue());

        return student.getID();
    }

    @Override
    public void delete(Student student) {
        String SQL = "DELETE FROM ученики WHERE Код_ученика=?";

        jdbcTemplate.update(SQL, student.getID());
    }
}

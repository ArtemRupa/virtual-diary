package com.VirtualDiary.DAO.implementsDAO;

import com.VirtualDiary.DAO.interfacesDAO.TeacherDAO;
import com.VirtualDiary.DAO.implementsDAO.RowMapperImpl.TeacherRowMapper;
import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.List;

@Component
public class TeacherDaoImpl implements TeacherDAO{

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private TableManager tableManager;

    @Override
    public List<Teacher> findByName(String Name) {
        String SQL = "SELECT * FROM преподаватели WHERE ФИО=?";

        List<Teacher> list = jdbcTemplate.query(SQL, new TeacherRowMapper(tableManager), Name);


        return list;
    }

    @Override
    public int insert(Teacher teacher) {

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
               PreparedStatement ps = connection.prepareStatement("INSERT INTO преподаватели (ФИО, Телефон, Категория, Классное_руководство)" +
                       " VALUES (?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
               ps.setString(1, teacher.getName());
               ps.setInt(2, teacher.getTelefon());
               ps.setInt(3, teacher.getCategory());
               ps.setString(4, "НЕТ");

                return ps;
            }
        }, keyHolder);

        teacher.setID(keyHolder.getKey().intValue());

        return teacher.getID();
    }

    @Override
    public void delete(Teacher teacher) {
        String SQL = "DELETE FROM преподаватели WHERE Код_преподавателя=?, Классное_руководство=НЕТ";

        jdbcTemplate.update(SQL, teacher.getID());

    }

    @Override
    public Teacher findByID(int ID) {
        String SQL = "SELECT * FROM преподаватели WHERE Код_преподавателя=?";
        List<Teacher> list = jdbcTemplate.query(SQL, new TeacherRowMapper(tableManager), ID);
        Teacher teacher = list.get(0);

        return teacher;
    }

    @Override
    public List<Teacher> findAll() {
        String SQL = "SELECT * FROM преподаватели";

        List<Teacher> list = jdbcTemplate.query(SQL, new TeacherRowMapper(tableManager));

        return list;
    }
}

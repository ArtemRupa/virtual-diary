package com.VirtualDiary.DAO.implementsDAO;

import com.VirtualDiary.DAO.interfacesDAO.ClassesDAO;
import com.VirtualDiary.DAO.implementsDAO.EnumClass.HasClassEnum;
import com.VirtualDiary.DAO.implementsDAO.RowMapperImpl.ClassesRowMapper;
import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Classes;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class ClassesDaoImpl implements ClassesDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private TableManager tableManager;

    @Override
    public Classes findByID(int ID) {
        String SQL = "SELECT * FROM классы WHERE Код_классного_руководителя=?";

        List<Classes> list = jdbcTemplate.query(SQL, new ClassesRowMapper(tableManager), ID);
        Classes classes = list.get(0);

        return classes;
    }

    @Override
    public List<Classes> findAll() {
        String SQL = "SELECT * FROM классы";

        List<Classes> list = jdbcTemplate.query(SQL, new ClassesRowMapper(tableManager));

        return list;
    }

    @Override
    public Classes findByName(String Name) {
        String SQL = "SELECT * FROM классы WHERE Название=?";

        List<Classes> list = jdbcTemplate.query(SQL, new ClassesRowMapper(tableManager), Name);

        Classes classes = list.get(0);

        return classes;
    }

    @Override
    public int insert(Teacher teacher, Classes classes) {
        if(teacher.getHasClass()== HasClassEnum.НЕТ){
            String SQL = "INSERT INTO классы (Код_классного_руководителя, Название) VALUES(?,?)";
            Object object [] ={teacher.getID(), classes.getName()};
            List<Object[]> list = new ArrayList();
            list.add(object);
            jdbcTemplate.batchUpdate(SQL, list);
            classes.setClassroomTeacher(teacher);
            return classes.getID();
        } else{
            System.out.println("Преподаватель уже ведет класс, выберите другого");
        }
        return 0;
    }

    @Override
    public void delete(Classes classes) {
        String SQL = "DELETE FROM классы WHERE Код_классного_руководителя=?";

        jdbcTemplate.update(SQL, classes.getID());
    }
}

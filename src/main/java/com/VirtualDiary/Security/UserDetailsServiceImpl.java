package com.VirtualDiary.Security;

import com.VirtualDiary.Application.AuthController;
import com.VirtualDiary.Application.StudentController;
import com.VirtualDiary.Application.TeacherController;
import com.VirtualDiary.model.DataBaseManagment.TableManager;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.Student;
import com.VirtualDiary.model.DataBaseManagment.modelFromDB.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.User.UserBuilder;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    TableManager tableManager;
    @Autowired
    AuthController authController;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = tableManager.getUsersDao().findUsers(username);

        UserBuilder builder;
        if (user != null) {
            builder = org.springframework.security.core.userdetails.User.withUsername(username);
            builder.password(new BCryptPasswordEncoder().encode(user.getPassword()));
            builder.roles(user.getRole().toString());
            authController.setUser(user);       //передаем в контроллер авторизации юзера
        } else {
            throw new UsernameNotFoundException("User not found.");
        }

        return builder.build();
    }
}

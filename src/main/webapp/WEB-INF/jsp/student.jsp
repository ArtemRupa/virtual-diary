<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>VirtualDiary/student</title>
<%--    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/css/style.css"/>--%>
</head>
<body>
<h1>Добро пожаловать, ${name}</h1>

<div>
    <span>Ученик: ${name}</span>
    <br>
    <span>Класс: ${classes}</span>
    <br>
    <span>Классный руководитель: ${boss}</span>
</div>

<h2>Ваши оценки</h2>

<table border="1">
    <thead>
        <tr>
            <td>Предмет/Дата</td>
            <td>Преподаватель</td>
            <c:forEach items="${DataList}" var="dataset">
                <td>${dataset}</td>
            </c:forEach>
            <td>Средняя оценка</td>
        </tr>
    </thead>
    <body>
        <c:forEach items="${Lessons}" var="lessons">
            <tr>
                <td>${lessons.lesson.nameLesson}</td>
                <td>${lessons.teacher.name}</td>
                    <c:forEach items="${GradeMap.get(lessons.lesson.ID)}" var="gradeMap">
                        <td>${gradeMap.getGrade()}</td>
                    </c:forEach>
                <td>${AvarageGrade.get(lessons.lesson.ID)}</td>
            </tr>
        </c:forEach>
    </body>
</table>
<br>
<form action="/logout" method="post">
    <button type="submit" value="Sign Out">
        <strong>Выйти</strong>
    </button>
</form>

</body>

</html>
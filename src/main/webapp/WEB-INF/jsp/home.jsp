<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Virtual Diary</title>
</head>
<body>

<h1 align="center">Добро пожаловать в электронный дневник!</h1>

<span>Электронный дневник "Virtual Diary" - это часть информационной системы "школа", обеспечивающий доступ
к базе данных с оценками учеников. Веб-приложение позволяет преподавателям выставлять ученикам оценки
в удобной форме, а ученикам просматривать их в любое время. Для продолжения войдите как ученик или преподаватель.</span>
<br>
<br>
<div>
    <span>Возможности веб-приложения:</span>
    <ul>
        <li>Внесение оценок в БД mySQL</li>
        <li>Просмотр данных преподавателей или учеников</li>
        <li>Просмотр оценок</li>
        <li>Авторизация с использованием ролей</li>
    </ul>
<br>
    <span>В процессе разработки:</span>
    <ul>
        <li>Просмотр расписания</li>
        <li>Чат с преподавателем</li>
        <li>Просмотр домашних заданий</li>
        <li>Комментарии к оценке</li>
        <li>Оригинальный дизайн</li>
    </ul>
</div>

<a href="/login" target="_blank" align="center">Войти</a>

<br>
<span>Version 1.0, Author: Rupa A., 2019</span>

</body>
</html>

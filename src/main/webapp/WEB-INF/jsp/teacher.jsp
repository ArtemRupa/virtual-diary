<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE HTML>
<html>


<head>
    <%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>VirtualDiary/teacher</title>
    <script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <meta name="_csrf" th:content="${_csrf.token}"/> <meta name="_csrf_header" th:content="${_csrf.headerName}"/>


    <script>$( document ).ready(function(){     //Функция ajax для вывода полей select, где указаны уроки
         $( "#nameClassesSelect" ).click(function(){ // задаем функцию при нажатиии на элемент <button>
            $.ajax({
                method: "post", // метод HTTP, используемый для запроса
                url: "/getClassesFromForm", // строка, содержащая URL адрес, на который отправляется запрос
                dataType: 'json',
                data : {
                    nameClasses: $("#nameClassesSelect").val()
                },
                success:  function (response) {
                        if (response.status=="success") {
                            $('#nameLessonSelect').empty();
                            $.each(response.data, function (i, Lesson) {
                                $('#nameLessonSelect').append('<option value="' + Lesson.nameLesson + '">' + Lesson.nameLesson + '</option>');
                            })
                        }
                    },

                statusCode: {
                    200: function () { // выполнить функцию если код ответа HTTP 200
                        console.log( "Ok" );
                    }
                }
            })
        });
    });</script>

    <script>$( document ).ready(function(){     //функция для передачи в контроллер информации о том, по какому предмету нужны оценки
        $( "#nameLessonSelect" ).click(function(){ // задаем функцию при нажатиии на элемент <button>
            $.ajax({
                method: "post", // метод HTTP, используемый для запроса
                url: "/getLessonFromForm", // строка, содержащая URL адрес, на который отправляется запрос
                dataType: 'json',
                data : {
                    nameLesson: $("#nameLessonSelect").val()

                },
                success:  function (response){
                    $('#gradeView').prop('disabled', false);
                }
            })
        });
    });</script>

    <script>$( document ).ready(function() {        //ajax запрос, отправляющий данные оценки, которую нужно изменить/добавить/удалить.Пока запрос не завершен кнопка "сохарнить изменения" заблокирована
            $("#gradeTableBody").on("change", "input", function () {
                $('#saveButton').prop('disabled', true);
                var dataKey = $(this).parent().index();
                var $tr = $(this).closest('tr');
                var studentKey = $tr.index();
                var grade;
                if ($(this).val()!="")
                {grade = $(this).val();}
                else {grade=0;}
                $.ajax({
                    method: "post", // метод HTTP, используемый для запроса
                    url: "/getChangedGradeFromForm", // строка, содержащая URL адрес, на который отправляется запрос
                    dataType: 'json',
                    data : {
                        dataKey: dataKey - 1,
                        studentKey: studentKey,
                        grade: grade
                    },
                    success:  function (response){
                        $('#saveButton').prop('disabled', false);
                    }

                })
            });
        });</script>



</head>

<body>

<h1>Добро пожаловать, ${TeacherName}</h1>

<div>
    <span>Преподаватель: ${TeacherName}</span>
    <br>
    <span>Категория: ${Category}</span>
</div>

<div>
    <br>
    <span>Здесь вы можете изменять оценки своих учеников. У вас есть доступ только к классам, в которых вы ведете уроки.</span>
    <br>
    <span>Также вы можете просмотреть оценки класса, классным руководителем которого вы являетесь.</span>
    <br>
    <br>
<form:form onsubmit="return false;" method="post" modelAttribute="Classes">
        <form:select path="name" id="nameClassesSelect">
            <form:options items="${listOfClassesNames}"></form:options>
        </form:select>
        <select id="nameLessonSelect"></select>
</form:form>
    <br>
<form:form method="post" action="getGrade">
    <button type="submit" id="gradeView" disabled="true">
        <strong>Показать оценки</strong>
    </button>
</form:form>
    <br>
</div>


<strong>${readmeGrade}</strong>
<br>
<table border="1" id="gradeTableBody">
    <thead>
    <tr>
        <td>Ученик/Дата</td>
        <c:forEach items="${DataList}" var="dataset">
            <td>${dataset}</td>
        </c:forEach>
        <td>Семестровая оценка</td>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${Students}" var="student">
        <tr>
            <td>${student.getName()}</td>
            <c:forEach items="${GradeMap.get(student.getID())}" var="gradeMap">
                <td><input value=${gradeMap.getGrade()}></td>
            </c:forEach>
            <td>${AvarageGrade.get(student.getID())}</td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<br>
    <form:form method="post" action="saveGrade">
    <button type="submit" disabled="true" id="saveButton">
        <strong>Сохранить изменения</strong>:
    </button>
    </form:form >
<br>
    <span>Оценка за семестр округляется исходя из средней оценки ученика</span>
<br>
<form action="/logout" method="post">
    <button type="submit" value="Sign Out">
        <strong>Выйти</strong>
    </button>
</form>

</body>
